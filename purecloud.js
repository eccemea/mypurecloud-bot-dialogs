
const platformClient = require('purecloud-platform-client-v2');
const client = platformClient.ApiClient.instance;

const CLIENTID = '77a99973-af95-4f0a-8f3a-43e50cd8a11e'; // EMEADemo
const CLIENTSECRET = 'bAlTby92JXLS-8IicalfaSpD8L8889FChnHATrTY97g';
const ENV = 'mypurecloud.ie';



// Login to the PureCloud and retreive Users and Queues
function login(env, clientId, clientSecret) {
    console.log('Try to login to the PureCloud...');


    return new Promise(function (resolve, reject) {
        try {
            // Node.js - Client credentials strategy

            client.setEnvironment(env);
            client.loginClientCredentialsGrant(clientId, clientSecret)
                .then(function () {
                    console.log('Service is connected to PureCloud!');
                    resolve();

                })
                .catch(function (err) {
                    console.log(JSON.stringify(err));
                    reject(false);
                });

        } catch (err) {
            console.log(err);
            reject(false);
        }
    });
}



function insertNewContactToCallList() {
    console.log('insertNewContactToCallList');
    let apiInstance = new platformClient.OutboundApi();

    let contactListId = "29e79100-781f-4fe8-89cb-e710087406e4"; // String | Contact List ID
    let body = [{  
        "contactListId": contactListId,
        "data": {
            "Account Number": "0030Y00001cqSvl",
            "Title": "Mr",
            "Forename": "Karsten",
            "Initial": "KF",
            "Surname": "Fisher",
            "dest": "+447503584524",
            "confirmation": "Y"
        },
        "callable": true
    }];
    let opts = {
        'priority': true, // Boolean | Contact priority. True means the contact(s) will be dialed next; false means the contact will go to the end of the contact queue.
        'clearSystemData': true, // Boolean | Clear system data. True means the system columns (attempts, callable status, etc) stored on the contact will be cleared if the contact already exists; false means they won't.
        'doNotQueue': false // Boolean | Do not queue. True means that updated contacts will not have their positions in the queue altered, so contacts that have already been dialed will not be redialed. For new contacts they will not be called until a campaign recycle; False means that updated contacts will be re-queued, according to the 'priority' parameter.
    };

    apiInstance.postOutboundContactlistContacts(contactListId, body, opts)
        .then((data) => {
            console.log(`postOutboundContactlistContacts success! data: ${JSON.stringify(data, null, 2)}`);
        })
        .catch((err) => {
            console.log('There was a failure calling postOutboundContactlistContacts');
            console.error(err.text);
        });

}


exports.pushRecord = async function () {
    login(ENV, CLIENTID, CLIENTSECRET).then(function (resp) {

        insertNewContactToCallList().then(function(resp) {
        }).catch(function (err) {
        });

    }).catch(function (err) {
       
    });
}

