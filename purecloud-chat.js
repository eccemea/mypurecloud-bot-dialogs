
//#region require section

const request = require('request');                             // used to send https requests
const WebSocket = require('ws');                                // used to subscribe to web socket

//#endregion /require section

//#region PureCloud org settings

// EMEADemo
/*
const organizationId = process.env.PC_ORGANIZATIONID || "334eca88-f072-4981-96a5-5f2ca16b6fbf";  // organizationId
const deploymentId = process.env.PC_DEPLOYMENTID || "36e1bcf8-34d4-464e-a495-277a14533f05";    // deploymentId from PureCloud org definition
const queueName = process.env.PC_QUEUENAME || "HomeServe";                                      // queueName where Chat will be routed
const env = process.env.PC_ENV || 'mypurecloud.ie';                                   // PureCloud environment (mypurecloud.com / mypurecloud.ie)
*/


const organizationId = process.env.PC_ORGANIZATIONID || "5049461e-9547-41be-82ed-17f4961e9f1d";  // organizationId
const deploymentId = process.env.PC_DEPLOYMENTID || "c33e85a2-7be8-4d31-8a35-f3b43998f6ca";    // deploymentId from PureCloud org definition
const queueName = process.env.PC_QUEUENAME || "Apple";                                      // queueName where Chat will be routed
const env = process.env.PC_ENV || 'mypurecloud.ie';                                   // PureCloud environment (mypurecloud.com / mypurecloud.ie)




//#endregion /PureCloud org settings

// initiate a chat session with PureCloud
function startChat(_adapter, _context, _conversationState) {
    console.log('startChat');
    let myBody = {
        "organizationId": organizationId,
        "deploymentId": deploymentId,
        "routingTarget": {
            "targetType": "QUEUE",
            "targetAddress": queueName
        },
        "memberInfo": {
            "displayName": "Jon Snow",
            "profileImageUrl": "http://amaovs.xp3.biz/img/photo/sample-image.jpg",
            "customFields": {
                "firstName": "Jon",
                "lastName": "Snow"
            }
        }
    };

    let options = {
        url: 'https://api.' + env + '/api/v2/webchat/guest/conversations',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(myBody)
    };

    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var info = JSON.parse(body);
            console.log(info);

            webSocket = new WebSocket(info.eventStreamUri);
            webSocket.on('open', function () {
                //Connection is open. Start the subscription(s)
                console.log('WebSocket opened');
            });

            webSocket.on('message', function (message) {
                var data = JSON.parse(message);

                // we do not care about channel.metadata informations. Ignore them
                if (data.topicName == 'channel.metadata') {
                    return;
                }

                try {
                    if (_conversationState.purecloud == undefined && data.eventBody.member) {
                        console.log('do only once after chat session initated');
                        console.log(data.eventBody);
                        // do only once after chat session initated. Save particiapnt data and other required informations
                        _conversationState.purecloud = {};
                        _conversationState.purecloud.conversationId = data.eventBody.conversation.id;
                        _conversationState.purecloud.agentId = data.eventBody.member.id;
                        _conversationState.purecloud.botConversationId = _context.conversation.id;
                        _conversationState.purecloud.chatId = info.jwt;

                        console.log('conversationId', _conversationState.purecloud.conversationId);
                        console.log('agentId', _conversationState.purecloud.agentId);

                        // Send history
                        sendMessageToPureCloud(buildHistory(_conversationState.history), _conversationState.purecloud);
                    }

                    // new message from purecloud received
                    if (data.metadata.type == 'message') {

                        // We do not want to display message from the bot again (echo)
                        // display only messages where senderId is different than current botId
                        if (data.eventBody.sender.id != _conversationState.purecloud.agentId && data.eventBody.body.length > 0 ) {
                            console.log('msg from pc:', data.eventBody.body);
                            sendMessageToBot(_adapter, _context, data.eventBody.body);
                        }
                    // member-change event (detect DISCONNECT event)
                    } else if (data.metadata.type == 'member-change' && data.eventBody.member.id == _conversationState.purecloud.agentId && data.eventBody.member.state == 'DISCONNECTED') {
                        console.log('# chat disconnected, clear bot session');
                        _conversationState.purecloud = undefined;
                        _conversationState.history = undefined;

                        sendMessageToBot(_adapter, _context, '[purecloud disconnected]');
                    }
                } catch (error) {
                    console.log(error);
                }
            });
        } else {
            console.log(body);
            console.log(response.statusCode);
        }

    });
};

// send message to the bot
async function sendMessageToBot(_adapter, _context, _msg) {
    await _adapter.continueConversation(_context, async (proactiveTurnContext) => {
        await proactiveTurnContext.sendActivity(_msg);
    });
}

// send message to the purecloud
function sendMessageToPureCloud(_msg, _data) {
    console.log('sendMessageToPureCloud');
    
    let myBody = {
        body: _msg,
        bodyType: 'standard'
    };

    let options = {
        url: 'https://api.' + env + '/api/v2/webchat/guest/conversations/' + _data.conversationId + '/members/' + _data.agentId + '/messages',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer ' + _data.chatId
        },
        body: JSON.stringify(myBody)
    };

    console.log(options);

    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var info = JSON.parse(body);
            console.log('msg sent to pc:', _msg);

        } else {
            console.log(error);
        }
    });
}

// prepare chat history to be sent to the purecloud.
function buildHistory(_history) {
    let ret = '--- bot history ---\n'
    for (var _item in _history) {
        ret = ret + _history[_item] + '\n';
    }

    ret = ret + '--- bot history ---\n';
    return ret
}

module.exports = { startChat, sendMessageToPureCloud }