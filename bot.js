// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { ActivityTypes, ActionTypes, CardFactory, TurnContext } = require('botbuilder');
const pc = require('./purecloud-chat');
const pc_api = require('./purecloud');

const { DialogSet, WaterfallDialog, NumberPrompt, DateTimePrompt, ChoicePrompt, DialogTurnStatus }
    = require('botbuilder-dialogs');

// Define identifiers for our state property accessors.
const DIALOG_STATE_ACCESSOR = 'dialogStateAccessor';
const RESERVATION_ACCESSOR = 'reservationAccessor';

// Define identifiers for our dialogs and prompts.
const RESERVATION_DIALOG = 'reservationDialog';
const SIZE_RANGE_PROMPT = 'rangePrompt';
const LOCATION_PROMPT = 'locationPrompt';
const RESERVATION_DATE_PROMPT = 'reservationDatePrompt';

// Welcomed User property name
const WELCOMED_USER = 'welcomedUserProperty';

class DialogPromptBot {
    /**
     *
     * @param {ConversationState} conversation state object
     */
    constructor(conversationState, userState) {
        this.welcomedUserProperty = userState.createProperty(WELCOMED_USER);
        this.userState = userState;

        // Creates our state accessor properties.
        // See https://aka.ms/about-bot-state-accessors to learn more about the bot state and state accessors.
        this.dialogStateAccessor = conversationState.createProperty(DIALOG_STATE_ACCESSOR);
        this.reservationAccessor = conversationState.createProperty(RESERVATION_ACCESSOR);
        this.conversationState = conversationState;

        // Create the dialog set and add the prompts, including custom validation.
        this.dialogSet = new DialogSet(this.dialogStateAccessor);
        //this.dialogSet.add(new NumberPrompt(SIZE_RANGE_PROMPT, this.rangeValidator));
        //this.dialogSet.add(new ChoicePrompt(LOCATION_PROMPT));
        this.dialogSet.add(new DateTimePrompt(RESERVATION_DATE_PROMPT, this.dateValidator));

        // Define the steps of the waterfall dialog and add it to the set.
        this.dialogSet.add(new WaterfallDialog(RESERVATION_DIALOG, [
            //this.promptForPartySize.bind(this),
            // this.promptForLocation.bind(this),
            this.promptForReservationDate.bind(this),
            this.acknowledgeReservation.bind(this),
        ]));
    }

    /**
     *
     * @param {TurnContext} on turn context object.
     */
    async onTurn(_adapter, turnContext) {
        console.log('activity.type', turnContext.activity.type);

        switch (turnContext.activity.type) {

            case ActivityTypes.Message:

                const didBotWelcomedUser = await this.welcomedUserProperty.get(turnContext, false);
                if (didBotWelcomedUser === false) {
                    // The channel should send the user name in the 'From' object
                    let userName = turnContext.activity.from.name;
                    /*
                    await turnContext.sendActivity('You are seeing this message because this was your first message ever sent to this bot.');
                    await turnContext.sendActivity(`It is a good practice to welcome the user and provide personal greeting. For example, welcome ${userName}.`);
                    */


                    //await turnContext.sendActivity('[Azure] Welcome', userName);

                    // construct thumbnailCard
                    
                    const card = CardFactory.thumbnailCard('Welcome', ['https://www.valassis.com/images/ui/chatbot-icon.png'],
                        null, { text: 'Hello there! I can help you with organize an appointment with one of our specialist or transfer you to one of our Agents. \nIf you wish to talk to the live person just type - transfer.' });
                    
                   /*
                    const card = CardFactory.heroCard('Welcome', ['https://talentguard.com/wp-content/uploads/2017/07/Zurich-Insurance-Group-Logo.png'],
                        null, { text: 'Welcome to Zurich. \nI am the messenger and I will help you with your requests. I noticed you were looking at different products.\nHow can I help you?' });
                        */
                    // add card to Activity.
                    // Bot: 

                    const reply = { type: ActivityTypes.Message };
                    reply.attachments = [card];

                    // Send hero card to the user.
                    await turnContext.sendActivity(reply);

                    // Set the flag indicating the bot handled the user's first message.
                    await this.welcomedUserProperty.set(turnContext, true);
                    // Save state changes
                    await this.userState.saveChanges(turnContext);
                    break;
                }

                // clear history, do not log it
                console.log('>', turnContext.activity.text);
                if (turnContext.activity.text == '#clear') {
                    if (this.conversationState.history) {
                        this.conversationState.history = undefined;
                        this.conversationState.purecloud = undefined;
                        // clear dialog & dialog results

                        await this.reservationAccessor.set(
                            turnContext,
                            null);
                        await this.welcomedUserProperty.set(turnContext, false);

                        // Save state changes
                        await this.userState.saveChanges(turnContext);
                        await this.conversationState.saveChanges(turnContext, false);


                        await turnContext.sendActivity('history & state cleared');
                    } else {
                        await turnContext.sendActivity('history object already empty');
                    }

                    break;
                }
                if (turnContext.activity.text == '#dialer') {
                    pc_api.pushRecord();
                    await turnContext.sendActivity('outbound record has been created.');
                    break;
                }


                if (this.conversationState.purecloud) {

                    console.log('> route msg to purecloud');
                    pc.sendMessageToPureCloud(turnContext.activity.text, this.conversationState.purecloud);

                } else {
                    //Bot logic

                    if (!this.conversationState.history) {
                        this.conversationState.history = [];
                    }

                    this.conversationState.history.push('Customer:\t' + turnContext.activity.text);


                    //#region Frank DEMO

                    if (turnContext.activity.text == 'I have bought a new vehicle and need insurance') {
                        let sBotResponse = 'No problem, I would recommend the following product http://www.google.com'
                        this.conversationState.history.push('Bot:\t\t\t' + sBotResponse);
                        await turnContext.sendActivity(sBotResponse);
                        return;
                    }
                    if (turnContext.activity.text == 'Okay, how about insurance for a third driver?') {
                        let sBotResponse = 'Unfortunately, I have to pass you on to my colleague, an agent, for this question. Please wait a moment.'
                        this.conversationState.history.push('Bot:\t\t\t' + sBotResponse);
                        pc.startChat(_adapter, TurnContext.getConversationReference(turnContext.activity), this.conversationState);
                        await turnContext.sendActivity(sBotResponse);
                        return;
                    }

                    //#endregion Frank DEMO

                    if (turnContext.activity.text == 'transfer') {
                        // transfer to PureCloud.
                        pc.startChat(_adapter, TurnContext.getConversationReference(turnContext.activity), this.conversationState);
                        this.conversationState.history.push('Bot:\t\t\tPlease wait, I will connect you with the best skilled Agent...');
                        await turnContext.sendActivity('Please wait, I will connect you with the best skilled Agent...');
                        return;
                    }

                    // Get the current reservation info from state.
                    const reservation = await this.reservationAccessor.get(turnContext, null);

                    // Generate a dialog context for our dialog set.
                    const dc = await this.dialogSet.createContext(turnContext);

                    if (turnContext.activity.text == 'transfer') {
                        // transfer to PureCloud.

                        pc.startChat(_adapter, TurnContext.getConversationReference(turnContext.activity), this.conversationState);
                        this.conversationState.history.push('Bot:\t\t\tPlease wait, I will connect you with the best skilled Agent...');
                        await turnContext.sendActivity('Please wait, I will connect you with the best skilled Agent...');

                        return;
                    }

                    if (turnContext.activity.text == '#transfer') {
                        // transfer to PureCloud.
                        await turnContext.sendActivity('>context:');

                        return;
                    }


                    if (!dc.activeDialog && turnContext.activity.text == 'transfer') {
                        // Transfer to PureCloud
                        const buttons = [
                            { type: ActionTypes.PostBack, title: 'Yes', value: 'Transfer:Tech' },
                            { type: ActionTypes.PostBack, title: 'No', value: 'No' }

                        ];

                        // construct hero card.
                        const card = CardFactory.heroCard('Transfer to the Agent', ['https://www.invensis.net/blog/wp-content/uploads/2015/05/best-qualities-of-call-center-agent-Invensis.jpg'],
                            buttons, { text: 'Would you like to be transferred to an Agent?' });

                        // add card to Activity.
                        const reply = { type: ActivityTypes.Message };
                        reply.attachments = [card];

                        // Send hero card to the user.
                        await turnContext.sendActivity(reply);



                    } else if (turnContext.activity.text == 'No') {
                        // Send hero card to the user.
                        this.conversationState.history.push('Bot:\t\t\tThat is fine. Have a good AI conversation with me...');
                        await turnContext.sendActivity('That is fine. Have a good AI conversation with me');

                    }

                    else

                        if (!dc.activeDialog) {
                            // If there is no active dialog, check whether we have a reservation yet.                            
                            if (!reservation) {
                                // If not, start the dialog.
                                await dc.beginDialog(RESERVATION_DIALOG);
                            }
                            else {
                                // Otherwise, send a status message.
                                this.conversationState.history.push('Bot:\t\t\tRemember, we will see you on ' + reservation.date);
                                await turnContext.sendActivity(
                                    `Remember, we'll see you on ${reservation.date}.`);
                            }
                        }
                        else {
                            // Continue the dialog.
                            const dialogTurnResult = await dc.continueDialog();

                            // If the dialog completed this turn, record the reservation info.
                            if (dialogTurnResult.status === DialogTurnStatus.complete) {
                                await this.reservationAccessor.set(
                                    turnContext,
                                    dialogTurnResult.result);

                                // Send a confirmation message to the user.
                                /*await turnContext.sendActivity(
                                    `Your party of ${dialogTurnResult.result.size} is ` +
                                    `confirmed for ${dialogTurnResult.result.date} in ` +
                                    `${dialogTurnResult.result.location}.`); */
                                // Send a confirmation message to the user.
                                this.conversationState.history.push('Bot:\t\t\tYour meeting for ' + dialogTurnResult.result.date + ' is confirmed.');
                                pc_api.pushRecord();
                                await turnContext.sendActivity(
                                    `Your meeting for ${dialogTurnResult.result.date} is confirmed.`);

                                // send outbound record /hardcoded to HomeserveAlwaysOn Calllist


                            }
                        }
                }

                // Save the updated dialog state into the conversation state.
                await this.conversationState.saveChanges(turnContext, false);
                break;
            case ActivityTypes.EndOfConversation:
            case ActivityTypes.DeleteUserData:
                break;
            default:
                break;
        }
    }

    async promptForPartySize(stepContext) {
        // Prompt for the party size. The result of the prompt is returned to the next step of the waterfall.
        this.conversationState.history.push('Bot:\t\t\tHow many people is the reservation for?');
        return await stepContext.prompt(
            SIZE_RANGE_PROMPT, {
                prompt: 'How many people is the reservation for?',
                retryPrompt: 'How large is your party?',
                validations: { min: 3, max: 8 },
            });
    }


    /*
        async promptForLocation(stepContext) {
            // Record the party size information in the current dialog state.
            stepContext.values.size = stepContext.result;
    
            // Prompt for location.
            return await stepContext.prompt(LOCATION_PROMPT, {
                prompt: 'Please choose a location.',
                retryPrompt: 'Sorry, please choose a location from the list.',
                choices: ['Redmond', 'Bellevue', 'Seattle'],
            });
        }
    */
    async promptForReservationDate(stepContext) {
        // Record the location information in the current dialog state.
        //stepContext.values.location = stepContext.result.value;
        this.conversationState.history.push('Bot:\t\t\tPlease tell me when will the reservation be for?');
        return await stepContext.prompt(
            RESERVATION_DATE_PROMPT, {
                prompt: 'Please tell me when will the reservation be for?',
                retryPrompt: 'What time should we make your reservation for?'
            });
    }

    async acknowledgeReservation(stepContext) {
        // Retrieve the reservation date.
        const resolution = stepContext.result[0];
        const time = resolution.value || resolution.start;
        this.conversationState.history.push('Bot:\t\t\tThank you. We will confirm your reservation shortly.');
        // Send an acknowledgement to the user.
        await stepContext.context.sendActivity(
            'Thank you. We will confirm your reservation shortly.');

        // Return the collected information to the parent context.
        return await stepContext.endDialog({
            date: time
            //size: stepContext.values.size,
            //location: stepContext.values.location
        });
        // TODO: Create an outbound record


    }

    async rangeValidator(promptContext) {
        // Check whether the input could be recognized as an integer.
        if (!promptContext.recognized.succeeded) {
            await promptContext.context.sendActivity(
                "I'm sorry, I do not understand. Please enter the number of people in your party.");
            return false;
        }
        else if (promptContext.recognized.value % 1 != 0) {
            await promptContext.context.sendActivity(
                "I'm sorry, I don't understand fractional people.");
            return false;
        }

        // Check whether the party size is appropriate.
        var size = promptContext.recognized.value;
        if (size < promptContext.options.validations.min
            || size > promptContext.options.validations.max) {
            await promptContext.context.sendActivity(
                'Sorry, we can only take reservations for parties of '
                + `${promptContext.options.validations.min} to `
                + `${promptContext.options.validations.max}.`);
            await promptContext.context.sendActivity(promptContext.options.retryPrompt);
            return false;
        }

        return true;
    }

    async dateValidator(promptContext) {
        // Check whether the input could be recognized as an integer.
        if (!promptContext.recognized.succeeded) {
            await promptContext.context.sendActivity(
                "I'm sorry, I do not understand. Please enter the date or time for your reservation.");
            return false;
        }

        // Check whether any of the recognized date-times are appropriate,
        // and if so, return the first appropriate date-time.
        const earliest = Date.now() + (60 * 60 * 1000);
        let value = null;
        promptContext.recognized.value.forEach(candidate => {
            // TODO: update validation to account for time vs date vs date-time vs range.
            const time = new Date(candidate.value || candidate.start);
            if (earliest < time.getTime()) {
                value = candidate;
            }
        });
        if (value) {
            promptContext.recognized.value = [value];
            return true;
        }

        await promptContext.context.sendActivity(
            "I'm sorry, we can't take reservations earlier than an hour from now.");
        return false;
    }
}

module.exports.DialogPromptBot = DialogPromptBot;